.DEFAULT_GOAL := help
SHELL = /bin/bash

default: help

ifneq (,$(wildcard ./.env))
    include .env
	export $(shell sed 's/=.*//' ./.env)
endif

.PONY: build-docker
build: ## build docker image
	docker build . -t registry.gitlab.com/yonzin/docker-gcloud:latest

push: ## push docker image
	docker push registry.gitlab.com/yonzin/docker-gcloud:latest

help: ## display this message
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
